# ℒagrangian Kernel Module

![Плотность лагранжиана](mod_icon.png "Не имеющая физического смысла плотность лагранжиана")
Kernel Module for InnerCore with several API for ℒagrangian plugin and InnerCore mods.
Library in alpha development, use with caution.

## Capabilities

### Flags

Performance & 64 bit numbers supporting API for creating, combining, checking Flags.

### Low level manipulation of Localizations

Localizations manipulation API.

### Low level creative inventory editor (BROKEN, temporarily out of development)

API for Creative category & Creative tab creation and managing.

## Development

Recommended tools: \
    Visual Studio Code \
    ESLint and Clangd \
    LaTeX Workshop \
    Python 3.6 or higher \
    Node.js 10.15.1 or higher \
    TEXLive with LuaTeX, polyglossia and TIKZ packages

## Thanks

Zheka Smirnov for Inner Core \
Hunabis for VTABLE_FIND_OFFSET and VTABLE_CALL methods. \
All [contributors](graphs/contributors)
