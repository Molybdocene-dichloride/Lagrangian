#pragma once

#include <vector>
#include <unordered_map>
#include <string>
#include <algorithm>

#include <multiversion/client/graphics/Type.hpp>
#include <multiversion/client/graphics/Operation.hpp>
#include <multiversion/client/graphics/Icon.hpp>
#include <multiversion/client/graphics/Vertex.hpp>
//#include <mcpe/Tesselator.hpp>
#include <multiversion/client/graphics/Model.hpp>
#include <multiversion/LRenderState.hpp>

namespace lagrangian {
	namespace graphics {
        class LRenderPipeline {
            public:
            std::unordered_map<Indexes<long>, std::shared_ptr<VertexOperation<Operable>>, IndexesHash<long>> ops;

            void operate(LModel& vs, LRenderState& state);
        };
        namespace LRenderer {
            //Tessellator tess;

            void begin(); //To Do
            void tessellate(LModel& vs);
            void tessellate(LModel& vs, LRenderPipeline& p);
            void tessellate(LModel& vs, LRenderPipeline& p, LRenderState& state);
            void draw();
        }
    }
}