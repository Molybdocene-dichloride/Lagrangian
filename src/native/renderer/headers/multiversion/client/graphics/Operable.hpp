#pragma once

class Operable {
    virtual void clear() {};
};